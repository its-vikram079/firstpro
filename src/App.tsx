import './App.css';
import Table from './Table';
import Navbar from './components/Navbar';
import UsersTable from './components/UsersTable';

function App() {
  return (
    <div id='mainwrapper'>
      <aside></aside>
      <section>
      <main><Navbar/></main>
      <menu><UsersTable/></menu>
      </section>
     
    </div>
  );
}

export default App;

